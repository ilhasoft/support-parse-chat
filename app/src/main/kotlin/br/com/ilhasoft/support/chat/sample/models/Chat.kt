package br.com.ilhasoft.support.chat.sample.models

import android.os.Parcel
import android.os.Parcelable
import br.com.ilhasoft.support.chat.models.ParseChat
import br.com.ilhasoft.support.parse.kotlin.delegates.attribute
import com.parse.ParseUser

/**
 * Created by john-mac on 2/7/17.
 */
class Chat() : ParseChat(), Parcelable {

    var tag by attribute<String?>()

    companion object {
        @JvmField val CREATOR: Parcelable.Creator<Chat> = object : Parcelable.Creator<Chat> {
            override fun createFromParcel(source: Parcel): Chat = Chat(source)
            override fun newArray(size: Int): Array<Chat?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this() {
        readParcel(source)
    }

    override fun createIndividualChat(user1: ParseUser, user2: ParseUser): ParseChat {
        val chat = super.createIndividualChat(user1, user2) as Chat
        chat.tag = "Teste"
        return chat
    }

}