package br.com.ilhasoft.support.chat.sample.ui.create

import br.com.ilhasoft.support.chat.sample.databinding.ItemUserSelectBinding
import br.com.ilhasoft.support.parse.models.ParseUser
import br.com.ilhasoft.support.recyclerview.adapters.ViewHolder

/**
 * Created by john-mac on 2/1/17.
 */
class ParseUserSelectViewHolder(val binding: ItemUserSelectBinding, val presenter: CreateGroupPresenter) :
        ViewHolder<ParseUser>(binding.root) {
    override fun onBind(user: ParseUser?) {
        binding.user = user
        binding.presenter = presenter
    }
}