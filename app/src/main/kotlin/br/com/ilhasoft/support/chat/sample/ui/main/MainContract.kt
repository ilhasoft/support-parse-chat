package br.com.ilhasoft.support.chat.sample.ui.main

import br.com.ilhasoft.support.chat.models.ParseChat
import br.com.ilhasoft.support.core.mvp.BasicView
import com.parse.ParseUser

interface MainContract : BasicView {

    fun setChat(chat: ParseChat)

    fun openFragment()

    fun startChatWithUser(user: ParseUser)

    fun startChatCreation()

}