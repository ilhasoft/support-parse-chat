package br.com.ilhasoft.support.chat.sample.ui.main

import br.com.ilhasoft.support.chat.sample.databinding.ItemUserBinding
import br.com.ilhasoft.support.parse.models.ParseUser
import br.com.ilhasoft.support.recyclerview.adapters.ViewHolder

/**
 * Created by john-mac on 2/1/17.
 */
class ParseUserViewHolder(val binding: ItemUserBinding, val presenter: MainPresenter) : ViewHolder<ParseUser>(binding.root) {
    override fun onBind(user: ParseUser?) {
        binding.user = user
        binding.presenter = presenter
    }
}