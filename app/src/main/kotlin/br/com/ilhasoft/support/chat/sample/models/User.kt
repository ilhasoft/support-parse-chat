package br.com.ilhasoft.support.chat.sample.models

import br.com.ilhasoft.support.parse.kotlin.delegates.attribute
import br.com.ilhasoft.support.parse.models.ParseUser
import com.parse.ParseClassName

/**
 * Created by john-mac on 2/7/17.
 */
@ParseClassName("_User")
class User : ParseUser() {

    val name by attribute<String?>()

}