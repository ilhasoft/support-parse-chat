package br.com.ilhasoft.support.chat.sample

import android.app.Application
import br.com.ilhasoft.support.chat.models.ParseMessage
import br.com.ilhasoft.support.chat.sample.models.Chat
import br.com.ilhasoft.support.chat.sample.models.User
import br.com.ilhasoft.support.parse.models.ParseMedia
import br.com.ilhasoft.support.parse.models.ParseObject
import com.parse.Parse
import tgio.parselivequery.LiveQueryClient

/**
 * Created by john-mac on 2/1/17.
 */
class Application : Application() {

    override fun onCreate() {
        super.onCreate()

        ParseObject.registerSubclass(Chat::class.java)
        ParseObject.registerSubclass(ParseMessage::class.java)
        ParseObject.registerSubclass(ParseMedia::class.java)
        ParseObject.registerSubclass(User::class.java)
        Parse.initialize(Parse.Configuration.Builder(this)
                .server(getString(R.string.parse_server_url))
                .applicationId(getString(R.string.parse_application_id))
                .clientKey(getString(R.string.parse_client_key))
                .build())
        LiveQueryClient.init(getString(R.string.parse_socket_url), getString(R.string.parse_application_id), true)
    }

}