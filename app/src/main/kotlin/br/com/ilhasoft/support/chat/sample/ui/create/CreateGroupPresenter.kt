package br.com.ilhasoft.support.chat.sample.ui.create

import br.com.ilhasoft.support.chat.sample.models.Chat
import br.com.ilhasoft.support.chat.ui.create.group.CreateChatGroupPresenter
import com.parse.ParseUser

class CreateGroupPresenter : CreateChatGroupPresenter<CreateGroupContract, Chat>(CreateGroupContract::class.java,
        ParseUser.getCurrentUser(), Chat::class.java) {

    fun attach(view: CreateGroupContract): CreateGroupPresenter {
        super.attachView(view)
        return this
    }

    fun onClickSelectUser(user: ParseUser?) {
        addOrRemoveUser(user)
    }
}
