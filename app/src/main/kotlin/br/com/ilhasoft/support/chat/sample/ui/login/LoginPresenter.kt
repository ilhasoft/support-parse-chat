package br.com.ilhasoft.support.chat.sample.ui.login

import br.com.ilhasoft.support.chat.helpers.ObservableHelper
import br.com.ilhasoft.support.chat.sample.models.User
import br.com.ilhasoft.support.core.mvp.Presenter
import br.com.ilhasoft.support.parse.models.ParseUser
import rx.parse.ParseObservable

class LoginPresenter : Presenter<LoginContract>(LoginContract::class.java) {

    fun attach(view: LoginContract): LoginPresenter {
        super.attachView(view)
        checkUserLogin()
        return this
    }

    fun checkUserLogin() {
        if (ParseUser.getCurrentUser() != null) {
            view?.startMain()
        }
    }

    fun onClickSignIn(username: String, password: String) {
        ParseObservable.logIn(username, password)
                .compose { ObservableHelper.transformer(it, this) }
                .subscribe ({ view?.startMain() }, { signUpUser(createNewUser(password, username)) } )
    }

    fun signUpUser(user: ParseUser) {
        ParseObservable.signUp(user)
                .compose { ObservableHelper.transformer(it, this) }
                .subscribe ({ view?.startMain() }, { ObservableHelper.error(this, it) })
    }

    private fun createNewUser(password: String, username: String): ParseUser {
        val user = User()
        user.username = username
        user.email = username
        user.setPassword(password)
        return user
    }

}
