package br.com.ilhasoft.support.chat.sample.ui.login

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity

import br.com.ilhasoft.support.chat.sample.R
import br.com.ilhasoft.support.chat.sample.databinding.ActivityLoginBinding
import br.com.ilhasoft.support.chat.sample.ui.main.MainActivity
import br.com.ilhasoft.support.core.app.IndeterminateProgressDialog

class LoginActivity : AppCompatActivity(), LoginContract {

    val binding: ActivityLoginBinding by lazy {
        DataBindingUtil.setContentView<ActivityLoginBinding>(this, R.layout.activity_login)
    }

    val presenter: LoginPresenter by lazy {
        val presenter = LoginPresenter().attach(this)
        presenter.attachView(this)
        presenter
    }

    val progress: IndeterminateProgressDialog by lazy {
        val progress = IndeterminateProgressDialog(this)
        progress.setMessage("Aguarde...")
        progress
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.presenter = presenter
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onStop() {
        super.onStop()
        presenter.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun startMain() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    override fun showMessage(messageId: Int) {
        Snackbar.make(window.decorView.rootView, messageId, Snackbar.LENGTH_LONG).show()
    }

    override fun showMessage(message: CharSequence?) {
        Snackbar.make(window.decorView.rootView, message ?: "", Snackbar.LENGTH_LONG).show()
    }

    override fun showLoading() {
        progress.show()
    }

    override fun dismissLoading() {
        progress.dismiss()
    }

}
