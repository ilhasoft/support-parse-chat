package br.com.ilhasoft.support.chat.sample.ui.login

import br.com.ilhasoft.support.core.mvp.BasicView

interface LoginContract : BasicView {
    fun startMain()
}