package br.com.ilhasoft.support.chat.sample.ui.main

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.ilhasoft.support.chat.models.ParseChat

import br.com.ilhasoft.support.chat.sample.R
import br.com.ilhasoft.support.chat.sample.databinding.ActivityMainBinding
import br.com.ilhasoft.support.chat.sample.databinding.ItemUserBinding
import br.com.ilhasoft.support.chat.sample.models.Chat
import br.com.ilhasoft.support.chat.sample.ui.create.CreateGroupActivity
import br.com.ilhasoft.support.chat.ui.room.ChatRoomDelegate
import br.com.ilhasoft.support.chat.ui.room.OnCloseChatListener
import br.com.ilhasoft.support.chat.ui.room.fragment.ChatRoomFragment
import br.com.ilhasoft.support.core.app.IndeterminateProgressDialog
import br.com.ilhasoft.support.parse.models.ParseObject
import br.com.ilhasoft.support.parse.models.ParseUser
import br.com.ilhasoft.support.parse.widgets.AutoParseQueryAdapter
import br.com.ilhasoft.support.parse.widgets.ParseQueryFactory
import br.com.ilhasoft.support.recyclerview.adapters.OnCreateViewHolder
import com.parse.ParseQuery

class MainActivity : AppCompatActivity(), MainContract, OnCreateViewHolder<ParseUser, ParseUserViewHolder>,
    OnCloseChatListener {

    val binding: ActivityMainBinding by lazy {
        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
    }

    val usersAdapter: AutoParseQueryAdapter<ParseUser, ParseUserViewHolder> by lazy {
        AutoParseQueryAdapter<ParseUser, ParseUserViewHolder>(getUsersQuery(), this)
    }

    var delegate: ChatRoomDelegate? = null
    var chatFragment: ChatRoomFragment? = null

    private fun getUsersQuery() = ParseQueryFactory {
        ParseQuery.getQuery(ParseUser::class.java).whereNotEqualTo(ParseObject.KEY_OBJECT_ID, ParseUser.getCurrentUser().objectId)
    }

    val presenter: MainPresenter by lazy {
        MainPresenter().attach(this)
    }

    val progress: IndeterminateProgressDialog by lazy {
        val progress = IndeterminateProgressDialog(this)
        progress.setMessage("Aguarde...")
        progress
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.presenter = presenter
        binding.users.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = usersAdapter
        }
        usersAdapter.fetch()
        presenter.attachView(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        delegate?.onActivityResult(requestCode, resultCode, data)
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onStop() {
        super.onStop()
        presenter.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun showMessage(messageId: Int) {
        Snackbar.make(binding.root, messageId, Snackbar.LENGTH_LONG).show()
    }

    override fun showMessage(message: CharSequence?) {
        Snackbar.make(binding.root, message ?: "", Snackbar.LENGTH_LONG).show()
    }

    override fun showLoading() {
        progress.show()
    }

    override fun dismissLoading() {
        progress.dismiss()
    }

    override fun onCreateViewHolder(layoutInflater: LayoutInflater?, parent: ViewGroup?, viewType: Int): ParseUserViewHolder {
        return ParseUserViewHolder(ItemUserBinding.inflate(layoutInflater, parent, false), presenter)
    }

    override fun openFragment() {
        if (supportFragmentManager.findFragmentById(R.id.chatContent) == null) {
            binding.createChatRoom.visibility = View.GONE
            chatFragment = ChatRoomDelegate().createFragment()
            supportFragmentManager.beginTransaction()
                    .add(R.id.chatContent, chatFragment)
                    .addToBackStack(null)
                    .commit()
        }
    }

    override fun setChat(chat: ParseChat) {
        chatFragment?.loadChatId(chat.objectId)
    }

    override fun startChatWithUser(user: com.parse.ParseUser) {
        delegate = ChatRoomDelegate(ParseUser.getCurrentUser(), user)
                .setToolbarColor(android.R.color.darker_gray)
                .setAttachMediaIcon(R.drawable.ic_gallery_grey_24dp)
                .setSendMessageColor(android.R.color.holo_orange_dark)
                .setSendMessageHint(R.string.title_youtube_url)
                .setSendMessageIcon(R.drawable.ic_camera_alt_grey_24dp)
                .setOtherMessageBackground(android.R.color.black)
                .setMyMessageBackground(android.R.color.holo_red_dark)
                .setUserNameKey("name")
                .setChatClass(Chat::class.java)
                .setOnCloseChatListener(this)
        startActivityForResult(delegate?.create(this), 210)
    }

    override fun onCloseChat(chat: ParseChat?) {
        showMessage("Chat closed: ${chat?.objectId}" )
    }

    override fun startChatCreation() {
        startActivity(Intent(this, CreateGroupActivity::class.java))
    }
}
