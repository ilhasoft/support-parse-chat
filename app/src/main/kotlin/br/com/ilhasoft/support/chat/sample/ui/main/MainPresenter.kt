package br.com.ilhasoft.support.chat.sample.ui.main

import br.com.ilhasoft.support.chat.helpers.ObservableHelper
import br.com.ilhasoft.support.chat.models.ParseChat
import br.com.ilhasoft.support.core.mvp.Presenter
import br.com.ilhasoft.support.parse.models.ParseUser
import com.parse.ParseQuery
import rx.Observable
import rx.parse.ParseObservable
import java.util.*

class MainPresenter : Presenter<MainContract>(MainContract::class.java) {

    fun attach(view: MainContract): MainPresenter {
        super.attachView(view)
        return this
    }

    fun onClickOpenFragment() {
        view?.openFragment()
        loadRandomChat()
    }

    private fun loadRandomChat() {
        val query = ParseQuery.getQuery(ParseChat::class.java)
        ParseObservable.all(query).toList()
                .flatMap { Observable.just(it[Random().nextInt(it.size - 1)]) }
                .compose { ObservableHelper.transformer(it, this) }
                .subscribe({ view?.setChat(it) }, { ObservableHelper.error(this, it) })
    }

    fun onClickUser(user: ParseUser) {
        view?.startChatWithUser(user)
    }

    fun onClickCreateChat() {
        view?.startChatCreation()
    }

}
