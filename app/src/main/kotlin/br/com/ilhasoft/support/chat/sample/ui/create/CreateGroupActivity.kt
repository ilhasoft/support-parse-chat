package br.com.ilhasoft.support.chat.sample.ui.create

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.ViewGroup
import br.com.ilhasoft.support.chat.models.ParseChat

import br.com.ilhasoft.support.chat.sample.R
import br.com.ilhasoft.support.chat.sample.databinding.ActivityCreateGroupBinding
import br.com.ilhasoft.support.chat.sample.databinding.ItemUserSelectBinding
import br.com.ilhasoft.support.chat.ui.room.ChatRoomDelegate
import br.com.ilhasoft.support.core.app.IndeterminateProgressDialog
import br.com.ilhasoft.support.parse.models.ParseObject
import br.com.ilhasoft.support.parse.models.ParseUser
import br.com.ilhasoft.support.parse.widgets.AutoParseQueryAdapter
import br.com.ilhasoft.support.parse.widgets.ParseQueryFactory
import br.com.ilhasoft.support.recyclerview.adapters.OnCreateViewHolder
import br.com.ilhasoft.support.validation.Validator
import com.parse.ParseQuery

class CreateGroupActivity : AppCompatActivity(), CreateGroupContract, OnCreateViewHolder<ParseUser, ParseUserSelectViewHolder> {

    val binding: ActivityCreateGroupBinding by lazy {
        DataBindingUtil.setContentView<ActivityCreateGroupBinding>(this, R.layout.activity_create_group)
    }

    val usersAdapters: AutoParseQueryAdapter<ParseUser, ParseUserSelectViewHolder> by lazy {
        AutoParseQueryAdapter<ParseUser, ParseUserSelectViewHolder>(getUsersQuery(), this)
    }

    val presenter: CreateGroupPresenter by lazy {
        val presenter = CreateGroupPresenter().attach(this)
        presenter
    }

    val progress: IndeterminateProgressDialog by lazy {
        val progress = IndeterminateProgressDialog(this)
        progress.setMessage("Aguarde...")
        progress
    }

    val validator: Validator by lazy { Validator(binding) }

    private var createMenuItem: MenuItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.presenter = presenter
        binding.chat = presenter.chat

        binding.usersList.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = usersAdapters
        }
        usersAdapters.fetch()
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onStop() {
        super.onStop()
        presenter.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun showMessage(messageId: Int) {
        Snackbar.make(binding.root, messageId, Snackbar.LENGTH_LONG).show()
    }

    override fun showMessage(message: CharSequence?) {
        Snackbar.make(binding.root, message ?: "", Snackbar.LENGTH_LONG).show()
    }

    override fun showLoading() {
        progress.show()
    }

    override fun dismissLoading() {
        progress.dismiss()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_create_chat_group, menu)
        createMenuItem = menu?.findItem(R.id.create)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.create -> presenter.createOrUpdateGroupChat()
        }
        return true
    }

    override fun startNewChat(chat: ParseChat) {
        startActivity(ChatRoomDelegate(chat.objectId).create(this))
        finish()
    }

    override fun finishSuccessfully(chat: ParseChat) {
        setResult(RESULT_OK)
        finish()
    }

    override fun validateChatGroup(): Boolean {
        return validator.validate()
    }

    override fun disableSaveButton() {
        createMenuItem?.isEnabled = false
    }

    override fun enableSaveButton() {
        createMenuItem?.isEnabled = true
    }

    override fun onCreateViewHolder(layoutInflater: LayoutInflater?, parent: ViewGroup?, viewType: Int):
            ParseUserSelectViewHolder {
        return ParseUserSelectViewHolder(ItemUserSelectBinding.inflate(layoutInflater, parent, false), presenter)
    }

    private fun getUsersQuery() = ParseQueryFactory {
        ParseQuery.getQuery(ParseUser::class.java).whereNotEqualTo(ParseObject.KEY_OBJECT_ID, ParseUser.getCurrentUser().objectId)
    }

}
