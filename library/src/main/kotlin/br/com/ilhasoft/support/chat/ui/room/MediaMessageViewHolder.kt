package br.com.ilhasoft.support.chat.ui.room

import android.view.View
import br.com.ilhasoft.support.chat.models.ParseMessage
import br.com.ilhasoft.support.chat.ui.media.MediaAdapter
import br.com.ilhasoft.support.chat.ui.media.MediaViewHolder
import kotlinx.android.synthetic.main.item_media_chat_message.view.*

/**
 * Created by john-mac on 12/14/16.
 */
class MediaMessageViewHolder(val binding: View,
                             onMediaViewListener: MediaAdapter.OnMediaViewListener,
                             myMessageBackground: Int, otherMessageBackground: Int, userNameKey: String?) :
        MessageViewHolder(binding, myMessageBackground, otherMessageBackground, userNameKey) {

    val holder: MediaViewHolder by lazy {
        MediaViewHolder(binding.media, false, onMediaViewListener)
    }

    override fun bind(message: ParseMessage) {
        super.bind(message)
        message.media?.let { holder.bindView(it) }
    }
}