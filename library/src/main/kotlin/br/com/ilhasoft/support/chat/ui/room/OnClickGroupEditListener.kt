package br.com.ilhasoft.support.chat.ui.room

import br.com.ilhasoft.support.chat.models.ParseChat

/**
 * Created by john-mac on 2/6/17.
 */
interface OnClickGroupEditListener {
    fun onClickGroupEdit(chat: ParseChat?)
}