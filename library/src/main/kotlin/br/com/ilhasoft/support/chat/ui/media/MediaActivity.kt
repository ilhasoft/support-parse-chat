package br.com.ilhasoft.support.chat.ui.media

import android.support.v7.app.AppCompatActivity
import br.com.ilhasoft.support.parse.models.ParseMedia

/**
 * Created by john-mac on 12/13/16.
 */
abstract class MediaActivity : AppCompatActivity(), MediaAdapter.OnMediaViewListener {

    val mediaView: MediaViewDelegate by lazy { MediaViewDelegate(this, supportFragmentManager) }

    override fun onMediaView(medias: MutableList<ParseMedia>?, position: Int) {
        mediaView.onMediaView(medias, position)
    }

    override fun onFileMediaView(media: ParseMedia?) {
        mediaView.onFileMediaView(media)
    }

    override fun onAudioMediaView(media: ParseMedia?) {
        mediaView.onAudioMediaView(media)
    }

}