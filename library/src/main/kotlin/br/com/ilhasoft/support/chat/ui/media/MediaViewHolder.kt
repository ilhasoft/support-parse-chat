package br.com.ilhasoft.support.chat.ui.media

import android.net.Uri
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import br.com.ilhasoft.support.chat.R
import br.com.ilhasoft.support.chat.models.MediaMetadata
import br.com.ilhasoft.support.core.helpers.TimeFormatHelper
import br.com.ilhasoft.support.graphics.BitmapLoader
import br.com.ilhasoft.support.media.YoutubeHelper
import br.com.ilhasoft.support.parse.models.ParseFile
import br.com.ilhasoft.support.parse.models.ParseMedia
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_media.view.*

/**
 * Created by john-mac on 12/7/16.
 */
class MediaViewHolder(val view: View,
                      val editMode: Boolean,
                      val mediaList: MutableList<ParseMedia>?,
                      val onMediaViewListener: MediaAdapter.OnMediaViewListener?,
                      val onMediaChangeListener: MediaAdapter.OnMediaChangeListener?)
                        : RecyclerView.ViewHolder(view) {

    private var media: ParseMedia? = null

    constructor(view: View, editMode: Boolean, onMediaViewListener: MediaAdapter.OnMediaViewListener)
            : this(view, editMode, null, onMediaViewListener, null)

    init {
        view.remove.visibility = if (editMode) View.VISIBLE else View.GONE
        view.remove.setOnClickListener { if (editMode) removeMedia() }
        itemView.setOnClickListener { if (!editMode) onItemViewClickListener.onClick(it) }
    }

    fun bindView(media: ParseMedia) {
        this.media = media
        resetDefaults()

        media.mediaFile?.let {
            if (it is ParseFile) {
                bindLocalMedia(media)
            } else {
                bindRemoteMedia(media)
            }
        }
        view.videoPlay.visibility = if (media.type == "video" || media.type == "youtube") View.VISIBLE
            else View.GONE
    }

    private fun bindRemoteMedia(media: ParseMedia) {
        when (media.type) {
            ParseMedia.TYPE_YOUTUBE -> {
                val url = YoutubeHelper.getThumbnailUrlFromId(media.youtubeCode, YoutubeHelper.THUMBNAIL_HIGH)
                Picasso.with(itemView.context).load(url).into(view.image)
            }
            ParseMedia.TYPE_FILE -> bindFile(media)
            ParseMedia.TYPE_AUDIO -> bindAudio(media)
            else -> Picasso.with(itemView.context).load(media.thumbnail?.url)
                    .fit().centerCrop()
                    .placeholder(R.color.black_translucent)
                    .into(view.image)
        }
    }

    private fun bindLocalMedia(media: ParseMedia) {
        val localParseFile = media.mediaFile as? ParseFile
        val uri = Uri.fromFile(localParseFile?.localFile)
        when (media.type) {
            ParseMedia.TYPE_VIDEO -> BitmapLoader.loadBitmapByVideoPath(view.image, uri, 100)
            ParseMedia.TYPE_FILE -> bindFile(media)
            ParseMedia.TYPE_AUDIO -> bindAudio(media)
            else -> BitmapLoader.loadBitmapByFile(view.image, localParseFile?.localFile?.absolutePath, 100)
        }
    }

    private fun containsMetadata(media: ParseMedia, metadataKey: String): Boolean {
        return media.metadata?.containsKey(metadataKey) == true
    }

    private fun bindFile(media: ParseMedia) {
        if(containsMetadata(media, MediaMetadata.FILENAME)) {
            val filename = media.metadata?.get(MediaMetadata.FILENAME)
            view.name.text = filename
        }
        bindGeneric(R.drawable.ic_folder_white_24dp, R.color.orange)
    }

    private fun bindGeneric(imageResource: Int, color: Int) {
        view.image.scaleType = ImageView.ScaleType.CENTER_INSIDE
        view.image.setImageResource(imageResource)
        view.image.setBackgroundColor(ResourcesCompat.getColor(itemView.resources, color
                , itemView.context.theme))
    }

    private fun bindAudio(media: ParseMedia) {
        if(containsMetadata(media, MediaMetadata.AUDIO_DURATION)) {
            val duration = media.metadata?.get(MediaMetadata.AUDIO_DURATION)
            view.name.text = TimeFormatHelper.getTimeFormattedFromSeconds(duration?.toLong() ?: 0)
            view.name.gravity = Gravity.END
        }
        bindGeneric(R.drawable.ic_music_note_white_24dp, R.color.green)
    }

    private fun resetDefaults() {
        view.image.scaleType = ImageView.ScaleType.CENTER_CROP
        view.image.setBackgroundColor(ResourcesCompat.getColor(itemView.resources
                , android.R.color.transparent, itemView.context.theme))
        view.name.gravity = Gravity.CENTER_HORIZONTAL
        view.name.text = null
    }

    private val onItemViewClickListener = View.OnClickListener {
        when (this.media?.type) {
            ParseMedia.TYPE_FILE -> onMediaViewListener?.onFileMediaView(this.media)
            ParseMedia.TYPE_AUDIO -> onMediaViewListener?.onAudioMediaView(this.media)
            else -> onMediaViewListener?.onMediaView(mediaList ?: mutableListOf(this.media!!), layoutPosition)
        }
    }

    private fun removeMedia() {
        onMediaChangeListener?.onMediaRemove(getCorrectPosition(layoutPosition))
    }

    private fun getCorrectPosition(position: Int): Int = if (editMode) position - 1 else position

}