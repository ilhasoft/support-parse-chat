package br.com.ilhasoft.support.chat.ui.room

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.annotation.ColorRes
import android.support.annotation.DrawableRes
import android.support.annotation.StringRes
import br.com.ilhasoft.support.chat.models.ParseChat
import br.com.ilhasoft.support.chat.models.ParseMessage
import br.com.ilhasoft.support.chat.ui.room.activity.ChatRoomActivity
import br.com.ilhasoft.support.chat.ui.room.fragment.ChatRoomFragment
import com.parse.ParseUser

/**
 * Created by john-mac on 2/2/17.
 */
class ChatRoomDelegate {

    private var args: Bundle = Bundle()

    private var type: String = ChatRoomFragment.TYPE_USERS
    internal var chatId: String? = null
    internal var chat: ParseChat? = null

    internal var user1: ParseUser? = null
    internal var user2: ParseUser? = null

    private var onClickGroupEditListener: OnClickGroupEditListener? = null
    private var onClickChatInfoListener: OnClickChatInfoListener? = null
    private var onCloseChatListener: OnCloseChatListener? = null

    constructor(user1: ParseUser, user2: ParseUser) {
        this.user1 = user1
        this.user2 = user2
        this.type = ChatRoomFragment.TYPE_USERS
    }

    constructor(chat: ParseChat) {
        this.chat = chat
        this.type = ChatRoomFragment.TYPE_CHAT_LOADED
    }

    constructor(chatId: String) {
        this.chatId = chatId
        this.type = ChatRoomFragment.TYPE_CHAT_NOT_LOADED
    }

    constructor() {
        this.type = ChatRoomFragment.TYPE_WITHOUT_CHAT
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (resultCode) {
            ChatRoomActivity.RESULT_CODE_SHOW_INFO -> onClickChatInfoListener?.onClickChatInfo(getChatFromBundle(data))
            ChatRoomActivity.RESULT_CODE_EDIT_GROUP -> onClickGroupEditListener?.onClickGroupEdit(getChatFromBundle(data))
            else -> getChatFromBundle(data)?.let { onCloseChatListener?.onCloseChat(it) }
        }
    }

    private fun getChatFromBundle(data: Intent?) = data?.getParcelableExtra<ParseChat>(ChatRoomFragment.EXTRA_CHAT)

    fun setUserNameKey(userNameKey: String): ChatRoomDelegate {
        args.putString(ChatRoomFragment.ARG_USER_NAME_KEY, userNameKey)
        return this
    }

    fun setUserPictureKey(userPictureKey: String): ChatRoomDelegate {
        args.putString(ChatRoomFragment.ARG_USER_PICTURE_KEY, userPictureKey)
        return this
    }

    fun setUserDefaultPicture(@DrawableRes userDefaultPicture: Int): ChatRoomDelegate {
        args.putInt(ChatRoomFragment.ARG_USER_DEFAULT_PICTURE, userDefaultPicture)
        return this
    }

    fun setGroupDefaultPicture(@DrawableRes groupDefaultPicture: Int): ChatRoomDelegate {
        args.putInt(ChatRoomFragment.ARG_GROUP_DEFAULT_PICTURE, groupDefaultPicture)
        return this
    }

    fun setToolbarColor(@ColorRes toolbarColor: Int): ChatRoomDelegate {
        args.putInt(ChatRoomFragment.ARG_TOOLBAR_COLOR, toolbarColor)
        return this
    }

    fun setAttachMediaIcon(@DrawableRes attachMediaIcon: Int): ChatRoomDelegate {
        args.putInt(ChatRoomFragment.ARG_ATTACH_MEDIA_ICON, attachMediaIcon)
        return this
    }

    fun setSendMessageColor(@ColorRes sendMessageColor: Int): ChatRoomDelegate {
        args.putInt(ChatRoomFragment.ARG_SEND_MESSAGE_COLOR, sendMessageColor)
        return this
    }

    fun setSendMessageIcon(@DrawableRes sendMessageIcon: Int): ChatRoomDelegate {
        args.putInt(ChatRoomFragment.ARG_SEND_MESSAGE_ICON, sendMessageIcon)
        return this
    }

    fun setSendMessageHint(@StringRes sendMessageHint: Int): ChatRoomDelegate {
        args.putInt(ChatRoomFragment.ARG_SEND_MESSAGE_HINT, sendMessageHint)
        return this
    }

    fun setMyMessageBackground(@DrawableRes myMessageBackground: Int): ChatRoomDelegate {
        args.putInt(ChatRoomFragment.ARG_MY_MESSAGE_BACKGROUND, myMessageBackground)
        return this
    }

    fun setOtherMessageBackground(@DrawableRes otherMessageBackground: Int): ChatRoomDelegate {
        args.putInt(ChatRoomFragment.ARG_OTHER_MESSAGE_BACKGROUND, otherMessageBackground)
        return this
    }

    fun <T: ParseChat> setChatClass(chatClass: Class<T>): ChatRoomDelegate {
        args.putSerializable(ChatRoomFragment.ARG_CHAT_CLASS, chatClass)
        return this
    }

    fun <T: ParseMessage> setMessageClass(messageClass: Class<T>): ChatRoomDelegate {
        args.putSerializable(ChatRoomFragment.ARG_MESSAGE_CLASS, messageClass)
        return this
    }

    fun setOnCloseChatListener(onCloseChatListener: OnCloseChatListener): ChatRoomDelegate {
        this.onCloseChatListener = onCloseChatListener
        return this
    }

    fun setOnClickGroupEditListener(onClickGroupEditListener: OnClickGroupEditListener): ChatRoomDelegate {
        this.onClickGroupEditListener = onClickGroupEditListener
        args.putBoolean(ChatRoomFragment.ARG_SHOW_EDIT_GROUP, true)
        return this
    }

    fun setOnClickGroupInfoListener(onClickChatInfoListener: OnClickChatInfoListener): ChatRoomDelegate {
        this.onClickChatInfoListener = onClickChatInfoListener
        args.putBoolean(ChatRoomFragment.ARG_SHOW_CHAT_INFO, true)
        return this
    }

    fun create(context: Context): Intent = when (type) {
        ChatRoomFragment.TYPE_USERS -> ChatRoomActivity.createWithUsers(context, user1, user2, args)
        ChatRoomFragment.TYPE_CHAT_NOT_LOADED -> ChatRoomActivity.createWithChatId(context, chatId, args)
        ChatRoomFragment.TYPE_CHAT_LOADED -> ChatRoomActivity.createWithChat(context, chat, args)
        else -> throw IllegalStateException("It's not possible to create a chat room with a unknown type")
    }

    fun createFragment(): ChatRoomFragment = when (type) {
        ChatRoomFragment.TYPE_USERS -> ChatRoomFragment.newInstance(user1, user2, args)
        ChatRoomFragment.TYPE_CHAT_NOT_LOADED -> ChatRoomFragment.newInstance(chatId, args)
        ChatRoomFragment.TYPE_CHAT_LOADED -> ChatRoomFragment.newInstance(chat, args)
        ChatRoomFragment.TYPE_WITHOUT_CHAT -> ChatRoomFragment.newInstance(args)
        else -> throw IllegalStateException("It's not possible to create a chat room with a unknown type")
    }

}
