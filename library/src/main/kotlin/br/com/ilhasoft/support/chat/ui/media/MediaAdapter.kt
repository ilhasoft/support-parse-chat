package br.com.ilhasoft.support.chat.ui.media

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.ilhasoft.support.chat.R

import br.com.ilhasoft.support.parse.models.ParseMedia

/**
 * Created by johncordeiro on 7/15/15.
 */
class MediaAdapter(var mediaList: MutableList<ParseMedia>, val editMode: Boolean)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private val MEDIA_TYPE = 0

        private val ADD_MEDIA_TYPE = 1
        private val ADD_MEDIA_ITEM_ID: Long = 1000
    }

    var onMediaChangeListener: OnMediaChangeListener? = null
    var onMediaViewListener: OnMediaViewListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder? {
        val inflater = LayoutInflater.from(parent.context)
        when (viewType) {
            MEDIA_TYPE -> return MediaViewHolder(inflater.inflate(R.layout.item_media, parent, false), editMode,
                    mediaList, onMediaViewListener, onMediaChangeListener)
            ADD_MEDIA_TYPE -> return AddMediaViewHolder(inflater.inflate(R.layout.item_add_media, parent, false))
        }
        return null
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            MEDIA_TYPE -> {
                val mediaViewHolder = holder as MediaViewHolder
                mediaViewHolder.bindView(mediaList[getCorrectPosition(position)])
            }
        }
    }

    private fun getCorrectPosition(position: Int): Int = if (editMode) position - 1 else position

    override fun getItemId(position: Int): Long {
        if (getItemViewType(position) == ADD_MEDIA_TYPE) {
            return ADD_MEDIA_ITEM_ID
        }
        val media = mediaList[position - 1]
        return media.objectId?.hashCode()?.toLong() ?: generateLocalId(media)
    }

    fun generateLocalId(media: ParseMedia): Long {
        return when (media.type) {
            ParseMedia.TYPE_YOUTUBE -> media.youtubeCode?.hashCode()?.toLong()
            else -> media.mediaFile?.hashCode()?.toLong()
        } ?: media.toString().hashCode().toLong()
    }

    override fun getItemViewType(position: Int): Int {
        if (editMode && position == 0)
            return ADD_MEDIA_TYPE
        return MEDIA_TYPE
    }

    override fun getItemCount(): Int {
        if (editMode)
            return mediaList.size + 1
        else
            return mediaList.size
    }

    fun setMedias(medias: MutableList<ParseMedia>) {
        this.mediaList = medias
        notifyDataSetChanged()
    }

    fun addMedia(media: ParseMedia) {
        this.mediaList.add(media)
        notifyDataSetChanged()
    }

    fun removeFromPosition(position: Int) {
        this.mediaList.removeAt(position)
        notifyDataSetChanged()
    }

    private inner class AddMediaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener { onMediaChangeListener?.onMediaAdd() }
        }
    }

    interface OnMediaChangeListener {
        fun onMediaRemove(position: Int)
        fun onMediaAdd()
    }

    interface OnMediaViewListener {
        fun onMediaView(medias: MutableList<ParseMedia>?, position: Int)
        fun onFileMediaView(media: ParseMedia?)
        fun onAudioMediaView(media: ParseMedia?)
    }

}
