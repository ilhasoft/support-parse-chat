package br.com.ilhasoft.support.chat.ui.room

import android.view.Gravity
import android.view.View
import kotlinx.android.synthetic.main.item_text_chat_message.view.*
import br.com.ilhasoft.support.chat.models.ParseMessage

/**
 * Created by john-mac on 12/14/16.
 */
class TextMessageViewHolder(view: View, myMessageBackground: Int, otherMessageBackground: Int, userNameKey: String?) :
        MessageViewHolder(view, myMessageBackground, otherMessageBackground, userNameKey) {

    override fun bind(message: ParseMessage) {
        super.bind(message)
        view.messageText.apply {
            gravity = if (message.isMyMessage()) Gravity.END else Gravity.START
            setTextColor(getColor(message))
            text = message.text
        }
    }

}