package br.com.ilhasoft.support.chat.helpers;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.support.design.widget.FloatingActionButton;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.widget.ImageButton;
import android.widget.ImageView;

/**
 * Created by john-mac on 11/29/16.
 */
public class ImageViewBindings {

    @BindingAdapter({"srcCompat"})
    public static void loadVectorImage(FloatingActionButton view, int resource) {
        Drawable drawable = VectorDrawableCompat.create(view.getResources(), resource
                , view.getContext().getTheme());
        view.setImageDrawable(drawable);
    }

    @BindingAdapter({"srcCompat"})
    public static void loadVectorImageButton(ImageButton view, int resource) {
        Drawable drawable = VectorDrawableCompat.create(view.getResources(), resource
                , view.getContext().getTheme());
        view.setImageDrawable(drawable);
    }

    @BindingAdapter({"srcCompat"})
    public static void loadVectorImageView(ImageView view, int resource) {
        Drawable drawable = VectorDrawableCompat.create(view.getResources(), resource
                , view.getContext().getTheme());
        view.setImageDrawable(drawable);
    }

}
