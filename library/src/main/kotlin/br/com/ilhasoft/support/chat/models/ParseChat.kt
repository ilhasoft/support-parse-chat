package br.com.ilhasoft.support.chat.models

import android.os.Parcel
import android.os.Parcelable
import android.support.annotation.StringDef
import br.com.ilhasoft.support.parse.kotlin.delegates.booleanAttribute
import br.com.ilhasoft.support.parse.kotlin.delegates.intAttribute
import br.com.ilhasoft.support.parse.kotlin.delegates.safeAttribute
import br.com.ilhasoft.support.parse.kotlin.extension.include
import br.com.ilhasoft.support.parse.kotlin.extension.whereContainsAll
import br.com.ilhasoft.support.parse.kotlin.extension.whereEqualTo
import br.com.ilhasoft.support.parse.models.ParseObject
import com.parse.ParseClassName
import com.parse.ParseFile
import com.parse.ParseQuery
import com.parse.ParseUser
import rx.Observable
import rx.parse.ParseObservable
import java.text.DateFormat
import java.util.*

/**
 * Created by developer on 05/12/16.
 */
@ParseClassName("Chat")
open class ParseChat() : ParseObject(), Parcelable {

    var users by safeAttribute<MutableList<ParseUser>?>()
    var title by safeAttribute<String?>()
    var subject by safeAttribute<String?>()
    @ChatType
    var type by safeAttribute<String?>()
    var administrator by safeAttribute<ParseUser?>()
    var lastMessage by safeAttribute<ParseMessage?>()
    var enabled by booleanAttribute()
    var media by safeAttribute<ParseFile?>()
    var unreadMessages by intAttribute()

    open fun getDateLastMessage(): String? {
        return DateFormat.getTimeInstance(DateFormat.SHORT)
                .format(lastMessage?.createdAt ?: createdAt)
    }

    open fun getName() = title

    open fun isPrivateGroup(): Boolean = type == TYPE_PRIVATE_GROUP

    open fun isIndividual(): Boolean = type == TYPE_INDIVIDUAL

    open fun isCurrentUserInChat() = users?.contains(ParseUser.getCurrentUser()) == true

    fun getUsersWithoutData(): MutableList<ParseUser>? {
        val usersWithoutData = ArrayList<ParseUser>()
        users?.forEach { usersWithoutData.add(ParseUser.createWithoutData(ParseUser::class.java, it.objectId)) }
        return if (usersWithoutData.isNotEmpty()) usersWithoutData else null
    }

    @Retention
    @StringDef(TYPE_INDIVIDUAL, TYPE_PUBLIC_GROUP, TYPE_PRIVATE_GROUP)
    annotation class ChatType

    companion object {
        const val TYPE_INDIVIDUAL = "individual"
        const val TYPE_PUBLIC_GROUP = "public_group"
        const val TYPE_PRIVATE_GROUP = "private_group"

        @JvmField val CREATOR: Parcelable.Creator<ParseChat> = object : Parcelable.Creator<ParseChat> {
            override fun createFromParcel(source: Parcel): ParseChat = ParseChat(source)
            override fun newArray(size: Int): Array<ParseChat?> = arrayOfNulls(size)
        }

        @JvmStatic fun removeUserFromGroupChat(chat: ParseChat?, user: ParseUser): Observable<ParseChat> {
            chat?.users?.remove(user)

            val chatUpdated = ParseObject.createWithoutData(ParseChat::class.java, chat?.objectId)
            chatUpdated.users = chat?.getUsersWithoutData()

            return ParseObservable.save(chatUpdated)
        }

        @JvmStatic fun addUserToGroupChat(chat: ParseChat?, user: ParseUser): Observable<ParseChat> {
            chat?.addUnique(ParseChat::users.name, user)

            val chatUpdated = ParseObject.createWithoutData(ParseChat::class.java, chat?.objectId)
            chatUpdated.users = chat?.getUsersWithoutData()

            return ParseObservable.save(chatUpdated)
        }

        @JvmStatic fun createUsersInChatQuery(chat: ParseChat): ParseQuery<ParseUser> {
            return ParseQuery.getQuery(ParseUser::class.java)
                    .whereContainedIn(KEY_OBJECT_ID, chat.users?.map { it.objectId })
        }

        @JvmStatic fun createChatByIdQuery(chatId: String, chatClass: Class<ParseChat>): ParseQuery<ParseChat> {
            return createBaseChatQuery(chatClass)
                    .whereEqualTo(KEY_OBJECT_ID, chatId)
        }

        @JvmStatic fun createChatsForCurrentUserQuery(chatClass: Class<ParseChat>): ParseQuery<ParseChat> {
            return createBaseChatQuery(chatClass)
                    .whereEqualTo(ParseChat::users, ParseUser.getCurrentUser())
        }

        @JvmStatic fun createAllGroupChatsQuery(chatClass: Class<ParseChat>): ParseQuery<ParseChat> {
            return createBaseChatQuery(chatClass)
                    .whereEqualTo(ParseChat::type, ParseChat.TYPE_PUBLIC_GROUP)
        }

        @JvmStatic fun fetchChatData(chat: ParseChat?, chatClass: Class<ParseChat>): ParseQuery<ParseChat> {
            return createBaseChatQuery(chatClass)
                    .whereEqualTo(KEY_OBJECT_ID, chat?.objectId)
        }

        @JvmStatic private fun createBaseChatQuery(chatClass: Class<ParseChat>): ParseQuery<ParseChat> {
            return ParseQuery.getQuery(chatClass)
                    .include(ParseChat::users)
                    .include(ParseChat::administrator)
                    .include(ParseChat::lastMessage)
                    .include(ParseChat::lastMessage, ParseMessage::media)
                    .addDescendingOrder(KEY_UPDATED_AT)
        }
    }

    open fun createIndividualChat(user1: ParseUser, user2: ParseUser): ParseChat {
        administrator = user1
        users = mutableListOf(user1, user2)
        type = ParseChat.TYPE_INDIVIDUAL
        return this
    }

    open fun createGroupChat(administrator: ParseUser): ParseChat {
        this.administrator = administrator
        this.users = mutableListOf(administrator)
        this.type = ParseChat.TYPE_PRIVATE_GROUP
        return this
    }

    open fun createIndividualChatQuery(users: MutableList<ParseUser>?, chatClass: Class<ParseChat>): ParseQuery<ParseChat> {
        return ParseQuery.getQuery(chatClass)
                .include(ParseChat::users)
                .include(ParseChat::administrator)
                .whereContainsAll(ParseChat::users, users as Collection<ParseUser>)
                .whereEqualTo(ParseChat::type, ParseChat.TYPE_INDIVIDUAL)
    }

    constructor(source: Parcel) : this() {
        readParcel(source)
    }

    fun readParcel(source: Parcel) {
        objectId = source.readString()
        val usersIds = mutableListOf<String>()
        source.readStringList(usersIds)
        users = usersIds.map { ParseUser.createWithoutData(ParseUser::class.java, it) }.toMutableList()
        title = source.readString()
        subject = source.readString()
        type = source.readString()
        administrator = ParseUser.createWithoutData(ParseUser::class.java, source.readString())
        lastMessage = source.readParcelable(ParseMessage::class.java.classLoader)
        enabled = source.readValue(Boolean::class.java.classLoader) as? Boolean ?: false
        media = source.readValue(ParseFile::class.java.classLoader) as? ParseFile
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(this.objectId)
        dest?.writeStringList(this.users?.map { it.objectId })
        dest?.writeString(this.title)
        dest?.writeString(this.subject)
        dest?.writeString(this.type)
        dest?.writeString(this.administrator?.objectId)
        dest?.writeParcelable(this.lastMessage, flags)
        dest?.writeValue(this.enabled)
        dest?.writeValue(this.media)
    }
}