package br.com.ilhasoft.support.chat.helpers

import android.util.Log
import br.com.ilhasoft.support.core.mvp.BasicView
import br.com.ilhasoft.support.parse.models.ParseFile as LocalParseFile
import com.parse.ParseException

import br.com.ilhasoft.support.core.mvp.Presenter
import br.com.ilhasoft.support.parse.ResponseHelper
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by daniel on 29/10/16.
 */
object ObservableHelper {

    fun <T, V : BasicView> transformer(observable: Observable<T>, presenter: Presenter<V>): Observable<T> {
        return observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { presenter.view.showLoading() }
                .doOnTerminate { presenter.view.dismissLoading() }
    }

    fun <V : BasicView> error(presenter: Presenter<V>, throwable: Throwable) {
        Log.e(presenter.javaClass.name, "Error executing task", throwable)
        if (throwable is ParseException) {
            presenter.view.dismissLoading()
            presenter.view.showMessage(ResponseHelper.getErrorMessageId(throwable))
        }
    }

    fun <V : BasicView> logError(presenter: Presenter<V>, throwable: Throwable) {
        Log.e(presenter.toString(), " Error on message", throwable)
    }

}
