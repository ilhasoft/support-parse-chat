package br.com.ilhasoft.support.chat.models

import android.os.Parcel
import android.os.Parcelable
import br.com.ilhasoft.support.parse.kotlin.delegates.booleanAttribute
import br.com.ilhasoft.support.parse.kotlin.delegates.safeAttribute
import br.com.ilhasoft.support.parse.kotlin.extension.include
import br.com.ilhasoft.support.parse.kotlin.extension.whereEqualTo
import br.com.ilhasoft.support.parse.kotlin.extension.whereNotEqualTo
import br.com.ilhasoft.support.parse.models.ParseMedia
import br.com.ilhasoft.support.parse.models.ParseObject
import com.parse.ParseClassName
import com.parse.ParseQuery
import com.parse.ParseUser
import java.text.DateFormat

/**
 * Created by developer on 05/12/16.
 */
@ParseClassName("Message")
open class ParseMessage() : ParseObject(), Parcelable {

    var user by safeAttribute<ParseUser?>()
    var text by safeAttribute<String?>()
    var media by safeAttribute<ParseMedia?>()
    var chat by safeAttribute<ParseChat?>()
    var chatId by safeAttribute<String?>()
    var enabled by booleanAttribute()

    fun getTimeFormatted(): String =
            DateFormat.getTimeInstance(DateFormat.SHORT).format(createdAt)

    fun isMyMessage(): Boolean = (user == ParseUser.getCurrentUser())

    companion object {
        @JvmField val CREATOR: Parcelable.Creator<ParseMessage> = object : Parcelable.Creator<ParseMessage> {
            override fun createFromParcel(source: Parcel): ParseMessage = ParseMessage(source)
            override fun newArray(size: Int): Array<ParseMessage?> = arrayOfNulls(size)
        }

        @JvmStatic fun createChatMessagesQuery(chat: ParseChat?, messageClass: Class<ParseMessage>): ParseQuery<ParseMessage> {
            return ParseQuery.getQuery(messageClass)
                    .include(ParseMessage::user)
                    .include(ParseMessage::media)
                    .whereEqualTo(ParseMessage::chat, chat)
                    .whereNotEqualTo(ParseMessage::enabled, false)
                    .orderByDescending(KEY_CREATED_AT)
        }
    }

    open fun createMessage(user: ParseUser): ParseMessage {
        this.user = user
        return this
    }

    constructor(source: Parcel) : this() {
        readParcel(source)
    }

    fun readParcel(source: Parcel) {
        objectId = source.readString()
        user = ParseUser.createWithoutData(ParseUser::class.java, source.readString())
        text = source.readString()
        media = source.readParcelable(ParseMedia::class.java.classLoader)
        chat = createWithoutData(ParseChat::class.java, source.readString())
        enabled = source.readValue(Boolean::class.java.classLoader) as? Boolean ?: false
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(this.objectId)
        dest?.writeString(this.user?.objectId)
        dest?.writeString(this.text)
        dest?.writeParcelable(this.media, flags)
        dest?.writeString(this.chat?.objectId)
        dest?.writeValue(enabled)
    }
}