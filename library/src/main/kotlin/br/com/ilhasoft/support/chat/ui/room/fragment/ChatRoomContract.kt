package br.com.ilhasoft.support.chat.ui.room.fragment

import android.content.Context
import android.support.annotation.StringRes
import br.com.ilhasoft.support.chat.models.ParseChat
import br.com.ilhasoft.support.chat.models.ParseMessage
import br.com.ilhasoft.support.core.mvp.BasicView
import rx.Observable

interface ChatRoomContract : BasicView {

    fun getFragmentContext(): Context

    fun pickMediaMessage()

    fun setupChat(chat: ParseChat)

    fun setMessages(messages: MutableList<ParseMessage>)

    fun addMessage(message: ParseMessage?)

    fun clearMessage()

}