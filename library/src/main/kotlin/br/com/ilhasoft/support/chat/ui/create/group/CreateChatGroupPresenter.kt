package br.com.ilhasoft.support.chat.ui.create.group

import br.com.ilhasoft.support.chat.helpers.ObservableHelper
import br.com.ilhasoft.support.chat.models.ParseChat
import br.com.ilhasoft.support.core.mvp.Presenter
import br.com.ilhasoft.support.parse.models.ParseObject
import com.parse.ParseUser
import rx.Observable
import rx.parse.ParseObservable

open class CreateChatGroupPresenter<View : CreateChatGroupContract, Chat: ParseChat>(viewClass: Class<View>,
                                                                    administrator: ParseUser,
                                                                    chatClass: Class<Chat>,
                                                                     var chat: Chat? = null)
    : Presenter<View>(viewClass) {

    init {
        if (chat == null) {
            chat = chatClass.newInstance()
        }

        if (chat?.objectId == null) {
            chat?.createGroupChat(administrator)
        }
    }

    fun addOrRemoveUser(user: ParseUser?) {
        val index = chat?.users?.indexOf(user) ?: -1
        if (index >= 0) {
            chat?.users?.removeAt(index)
        } else if (user != null) {
            chat?.users?.add(user)
        }
    }

    fun onPrivateCheckChanged(checked: Boolean) {
        chat?.type = if (checked) ParseChat.TYPE_PRIVATE_GROUP else ParseChat.TYPE_PUBLIC_GROUP
    }

    fun isUserSelected(user: ParseUser) = chat?.users?.indexOf(user) ?: -1 >= 0

    fun createOrUpdateGroupChat() {
        if (view?.validateChatGroup() == true) {
            chat?.let {
                val update = it.objectId != null
                val updatedChat = createChatForUpdate(it)

                view?.disableSaveButton()
                Observable.just(updatedChat)
                        .flatMap { saveMediaIfNeeded(it) }
                        .flatMap { ParseObservable.save(it) }
                        .compose { ObservableHelper.transformer(it, this) }
                        .doOnTerminate { view?.enableSaveButton() }
                        .subscribe ({ if (!update) view?.startNewChat(it) else view?.finishSuccessfully(it) },
                                { ObservableHelper.error(this, it) })
            }
        }
    }

    open protected fun createChatForUpdate(chat: ParseChat): ParseChat {
        if (chat.objectId != null) {
            val updatedChat = ParseObject.createWithoutData(ParseChat::class.java, chat.objectId)
            updatedChat.title = chat.title
            updatedChat.subject = chat.subject
            updatedChat.users = chat.getUsersWithoutData()
            updatedChat.media = chat.media
            updatedChat.type = chat.type
            return updatedChat
        }
        return chat
    }

    private fun saveMediaIfNeeded(chat: ParseChat): Observable<ParseChat>? {
        return if (chat.media != null && chat.media?.isDirty == true)
            ParseObservable.save(chat.media).flatMap { Observable.just(chat) }
        else Observable.just(chat)
    }

}
