package br.com.ilhasoft.support.chat.ui.room.fragment

import android.content.Context
import android.widget.EditText
import br.com.ilhasoft.support.chat.helpers.LiveQueryHelper
import br.com.ilhasoft.support.chat.helpers.ObservableHelper
import br.com.ilhasoft.support.chat.helpers.ParseMediaPresenter
import br.com.ilhasoft.support.chat.models.ParseChat
import br.com.ilhasoft.support.chat.models.ParseMessage
import br.com.ilhasoft.support.parse.models.ParseMedia
import br.com.ilhasoft.support.parse.models.ParseObject
import br.com.ilhasoft.support.parse.widgets.ParseQueryFactory
import com.parse.ParseUser
import org.json.JSONObject
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.parse.ParseObservable
import tgio.parselivequery.interfaces.OnListener

class ChatRoomPresenter(var chat: ParseChat?, val chatClass: Class<ParseChat>,
                        val messageClass: Class<ParseMessage>, context: Context?) :
        ParseMediaPresenter<ChatRoomContract>(ChatRoomContract::class.java, context),
        ParseQueryFactory<ParseMessage>,
        OnListener {

    fun attach(view: ChatRoomContract): ChatRoomPresenter {
        super.attachView(view)
        return this
    }

    override fun create() = ParseMessage.createChatMessagesQuery(chat, messageClass)

    fun loadChatWithUsers(user1: ParseUser, user2: ParseUser) {
        chat = chatClass.newInstance()
        ParseObservable.find(chat?.createIndividualChatQuery(mutableListOf(user1, user2), chatClass)).toList()
                .flatMap { results ->
                    if (results?.isEmpty() == true) {
                        ParseObservable.save(chat?.createIndividualChat(user1, user2))
                                .flatMap { ParseObservable.find(ParseChat.fetchChatData(it, chatClass)) }
                    } else {
                        Observable.just(results?.firstOrNull())
                    }
                }
                .doOnNext { chat = it }
                .flatMap { fetchUsers() }
                .compose { ObservableHelper.transformer(it, this) }
                .subscribe ({ view?.setupChat(chat!!) }, { ObservableHelper.error(this, it) })
    }

    fun loadChat(newChat: ParseChat?) {
        Observable.just(newChat)
                .doOnNext { chat = it }
                .flatMap { fetchUsers() }
                .compose { ObservableHelper.transformer(it, this) }
                .subscribe ({ view?.setupChat(chat!!) }, { ObservableHelper.error(this, it) })
    }

    fun loadChatById(chatId: String) {
        ParseObservable.find(ParseChat.createChatByIdQuery(chatId, chatClass))
                .doOnNext { chat = it }
                .flatMap { fetchUsers() }
                .compose { ObservableHelper.transformer(it, this) }
                .subscribe({ view?.setupChat(chat!!) }, { ObservableHelper.error(this, it) })
    }

    private fun fetchUsers() = if (chat?.users != null || chat?.users?.isNotEmpty() == true)
            ParseObservable.fetch(chat?.users).toList()
        else Observable.just(null)

    fun loadMessages() {
        ParseObservable.find(create()).toList()
                .compose { ObservableHelper.transformer(it, this) }
                .subscribe({ view?.setMessages(it) }, { ObservableHelper.error(this, it) })
    }

    fun onClickSendMessage(messageView: EditText) {
        if (messageView.length() > 0) {
            saveMessage(messageView.text.toString())
        } else {
            view?.pickMediaMessage()
        }
    }

    private fun saveMessage(content: String) {
        val message = createBaseMessage()
        message.text = content

        ParseObservable.save(message)
                .compose { ObservableHelper.transformer(it, this) }
                .subscribe({ view?.clearMessage() }, { ObservableHelper.error(this, it) })
    }

    override fun addMedia(media: ParseMedia) {
        val message = createBaseMessage()
        message.media = media
        message.chat = ParseObject.createWithoutData(ParseChat::class.java, message.chat?.objectId)

        saveMedia(view?.getFragmentContext(), message.media)
                .flatMap { ParseObservable.save(message) }
                .compose { ObservableHelper.transformer(it, this) }
                .subscribe ({ view?.clearMessage() }, { ObservableHelper.error(this, it) })
    }

    private fun createBaseMessage(): ParseMessage {
        val message = messageClass.newInstance()
        message.chat = this.chat
        message.createMessage(ParseUser.getCurrentUser())
        return message
    }

    override fun on(json: JSONObject?) {
        var message: ParseMessage? = null
        LiveQueryHelper.convert<ParseMessage>(json)
                .doOnNext { message = it }
                .flatMap { completeMessageWithUser(it) }
                .flatMap { if (it?.media != null) ParseObservable.fetch(it.media) else Observable.just(it) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ if (message?.chatId == chat?.objectId) view?.addMessage(message) },
                        { ObservableHelper.logError(this, it) })
    }

    private fun completeMessageWithUser(message: ParseMessage): Observable<ParseMessage>? {
        return Observable.just(message.user)
                .flatMap {
                    message.user?.let {
                        val indexOfUser = chat?.users?.indexOf(it) ?: -1
                        if (indexOfUser >= 0) {
                            Observable.just(chat?.users?.get(indexOfUser))
                        } else {
                            ParseObservable.fetch(it).doOnNext {
                                if (chat?.users == null) {
                                    chat?.users = mutableListOf()
                                }
                                chat?.users?.add(it)
                            }
                        }
                    }
                }.flatMap { message.user = it; Observable.just(message) }
    }

}
