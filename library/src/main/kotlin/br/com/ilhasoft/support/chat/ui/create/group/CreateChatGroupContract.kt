package br.com.ilhasoft.support.chat.ui.create.group

import br.com.ilhasoft.support.chat.models.ParseChat
import br.com.ilhasoft.support.core.mvp.BasicView
import br.com.ilhasoft.support.parse.models.ParseMedia

interface CreateChatGroupContract : BasicView {

    fun startNewChat(chat: ParseChat)

    fun finishSuccessfully(chat: ParseChat)

    fun validateChatGroup(): Boolean

    fun disableSaveButton()

    fun enableSaveButton()
}