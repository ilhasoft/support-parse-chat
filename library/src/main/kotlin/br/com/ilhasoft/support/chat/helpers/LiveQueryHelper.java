package br.com.ilhasoft.support.chat.helpers;

import com.parse.ParseObject;

import org.json.JSONObject;

import java.lang.reflect.Method;
import java.util.concurrent.Callable;

import rx.Observable;

/**
 * Created by john-mac on 12/14/16.
 */
public class LiveQueryHelper {

    @SuppressWarnings("unchecked")
    public static <Type extends ParseObject> Observable<Type> convert(final JSONObject json) {
        return Observable.fromCallable(new Callable<Type>() {
            @Override
            public Type call() throws Exception {
                Class parseDecoderClass = Class.forName("com.parse.ParseDecoder");
                Method fromJSONPayload = ParseObject.class.getDeclaredMethod("fromJSONPayload",
                                                                             JSONObject.class,
                                                                             parseDecoderClass);
                fromJSONPayload.setAccessible(true);
                Method getParseDecoder = parseDecoderClass.getDeclaredMethod("get");
                return (Type) fromJSONPayload.invoke(null, json.getJSONObject("object"),
                                                     getParseDecoder.invoke(null));
            }
        });
    }

}
