package br.com.ilhasoft.support.chat.models

/**
 * Created by john-mac on 1/31/17.
 */
object MediaMetadata {

    val AUDIO_DURATION = "audioDuration"
    val FILENAME = "filename"

}