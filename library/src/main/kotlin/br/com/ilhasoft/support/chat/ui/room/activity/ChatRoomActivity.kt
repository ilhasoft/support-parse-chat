package br.com.ilhasoft.support.chat.ui.room.activity

import android.app.Fragment
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.annotation.AttrRes
import android.support.annotation.StringRes
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import br.com.ihasoft.support.databinding.adapters.ImageViewBindingAdapter
import br.com.ilhasoft.support.chat.R
import br.com.ilhasoft.support.chat.models.ParseChat
import br.com.ilhasoft.support.chat.ui.room.fragment.ChatRoomContract
import br.com.ilhasoft.support.chat.ui.room.fragment.ChatRoomFragment
import com.parse.ParseFile
import com.parse.ParseUser
import kotlinx.android.synthetic.main.activity_chat_room.*
import rx.Observable

class ChatRoomActivity : AppCompatActivity(), ChatRoomActivityContract {

    val args: Bundle? by lazy { intent?.getBundleExtra(EXTRA_ARGS) }
    val type: String? by lazy { args?.getString(ChatRoomFragment.EXTRA_TYPE) }

    /** Interface attributes collected from args **/
    val userNameKey: String? by lazy { args?.getString(ChatRoomFragment.ARG_USER_NAME_KEY) }
    val userPictureKey: String? by lazy { args?.getString(ChatRoomFragment.ARG_USER_PICTURE_KEY) }
    val userDefaultPicture: Int by lazy { getResourceOrDefault(ChatRoomFragment.ARG_USER_DEFAULT_PICTURE, R.drawable.face) }
    val groupDefaultPicture: Int by lazy { getResourceOrDefault(ChatRoomFragment.ARG_GROUP_DEFAULT_PICTURE, R.drawable.ic_group_default) }
    val toolbarColor: Int by lazy { getColorArgOrPrimary(ChatRoomFragment.ARG_TOOLBAR_COLOR) }
    val showChatInfo: Boolean by lazy { args?.getBoolean(ChatRoomFragment.ARG_SHOW_CHAT_INFO, false) ?: false }
    val showEditGroup: Boolean by lazy { args?.getBoolean(ChatRoomFragment.ARG_SHOW_EDIT_GROUP, false) ?: false }

    val presenter: ChatRoomActivityPresenter by lazy { ChatRoomActivityPresenter(null).attach(this) }
    var chatFragmentContract: ChatRoomContract? = null

    companion object {
        const val EXTRA_ARGS = "args"

        const val RESOURCE_NOT_FOUND = 0

        const val RESULT_CODE_SHOW_INFO = 100
        const val RESULT_CODE_EDIT_GROUP = 101

        var open = false

        fun createWithChat(context: Context, chat: ParseChat?, args: Bundle): Intent {
            val chatIntent = Intent(context, ChatRoomActivity::class.java)
            args.putParcelable(ChatRoomFragment.EXTRA_CHAT, chat)
            args.putString(ChatRoomFragment.EXTRA_TYPE, ChatRoomFragment.TYPE_CHAT_LOADED)
            chatIntent.putExtra(EXTRA_ARGS, args)
            return chatIntent
        }

        fun createWithChatId(context: Context, chatId: String?, args: Bundle): Intent {
            val chatIntent = Intent(context, ChatRoomActivity::class.java)
            args.putString(ChatRoomFragment.EXTRA_CHAT_ID, chatId)
            args.putString(ChatRoomFragment.EXTRA_TYPE, ChatRoomFragment.TYPE_CHAT_NOT_LOADED)
            chatIntent.putExtra(EXTRA_ARGS, args)
            return chatIntent
        }

        fun createWithUsers(context: Context, user1: ParseUser?, user2: ParseUser?, args: Bundle): Intent {
            val chatIntent = Intent(context, ChatRoomActivity::class.java)
            args.putString(ChatRoomFragment.EXTRA_USER1, user1?.objectId)
            args.putString(ChatRoomFragment.EXTRA_USER2, user2?.objectId)
            args.putString(ChatRoomFragment.EXTRA_TYPE, ChatRoomFragment.TYPE_USERS)
            chatIntent.putExtra(EXTRA_ARGS, args)
            return chatIntent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_room)
        setupView()

        if (savedInstanceState == null && args != null) {
            supportFragmentManager.beginTransaction()
                    .add(R.id.chatContent, ChatRoomFragment.newInstance(args!!))
                    .commit()
        }
    }

    override fun onStart() {
        super.onStart()
        open = true
    }

    override fun onStop() {
        super.onStop()
        open = false
    }

    override fun onAttachFragment(fragment: Fragment?) {
        super.onAttachFragment(fragment)
        chatFragmentContract = fragment as? ChatRoomContract
    }

    fun getColorArgOrPrimary(arg: String): Int {
        val res = getResourceOrDefault(arg, RESOURCE_NOT_FOUND)
        return if (res != RESOURCE_NOT_FOUND) ContextCompat.getColor(this, res)
            else getThemeColor(R.attr.colorPrimary)
    }

    private fun getResourceOrDefault(argKey: String, defaultValue: Int) =
            args?.getInt(argKey, defaultValue) ?: defaultValue

    fun getThemeColor(@AttrRes attr: Int): Int {
        val value = TypedValue()
        theme.resolveAttribute(attr, value, true)
        return value.data
    }

    private fun setupView() {
        setSupportActionBar(supportChatToolbar)
        supportChatToolbar.setBackgroundColor(toolbarColor)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_chat_room, menu)
        menu?.findItem(R.id.info)?.isVisible = showChatInfo
        menu?.findItem(R.id.exit)?.isVisible = !presenter.hasAdminPrivileges() && presenter.isGroupChat()
        menu?.findItem(R.id.edit)?.isVisible = presenter.hasAdminPrivileges() && presenter.isGroupChat()
                && showEditGroup
        menu?.findItem(R.id.delete)?.isVisible = presenter.hasAdminPrivileges() && presenter.isGroupChat()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.info -> presenter.onClickInformation()
            R.id.edit -> presenter.onClickEditChat()
            R.id.delete -> presenter.onClickDeleteChat()
            R.id.exit -> presenter.onClickExitChat()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun startChatGroupInfo() {
        setResult(RESULT_CODE_SHOW_INFO, Intent().putExtra(ChatRoomFragment.EXTRA_CHAT, presenter.chat))
        finish()
    }

    override fun startChatGroupEdit() {
        setResult(RESULT_CODE_EDIT_GROUP, Intent().putExtra(ChatRoomFragment.EXTRA_CHAT, presenter.chat))
        finish()
    }

    override fun requestConfirmation(@StringRes message: Int): Observable<Boolean> {
        // TODO: Check if there is a better way to do this
        return Observable.unsafeCreate { subscriber ->
                AlertDialog.Builder(this)
                        .setMessage(message)
                        .setPositiveButton(R.string.yes, {
                            dialog, _ ->
                            dialog.dismiss()
                            subscriber.onNext(true)
                            subscriber.onCompleted()
                        }).setNegativeButton(R.string.no, null)
                        .setOnCancelListener { subscriber.onNext(false); subscriber.onCompleted() }
                .show()
        }
    }

    override fun showMessage(messageId: Int) {
        chatFragmentContract?.showMessage(messageId)
    }

    override fun showMessage(message: CharSequence?) {
        chatFragmentContract?.showMessage(message)
    }

    override fun showLoading() {
        chatFragmentContract?.showLoading()
    }

    override fun dismissLoading() {
        chatFragmentContract?.dismissLoading()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    override fun setupChat(chat: ParseChat) {
        presenter.chat = chat
        this.supportInvalidateOptionsMenu()
        this.setupChatView(chat)
        this.setResult(RESULT_OK, android.content.Intent().putExtra(ChatRoomFragment.EXTRA_CHAT, chat))
    }

    private fun setupChatView(chat: ParseChat) {
        supportChatPicture.setImageResource(getDefaultImage(chat))

        val pictureFile: ParseFile? = presenter.getChatPicture(chat, userPictureKey)
        pictureFile?.let { ImageViewBindingAdapter.loadImage(supportChatPicture, it.url, getDefaultImage(chat), getDefaultImage(chat), true) }
        supportChatTitle.text = presenter.getChatName(chat, userNameKey)
    }

    fun getDefaultImage(chat: ParseChat) = if (chat.isIndividual()) userDefaultPicture else groupDefaultPicture

}
