package br.com.ilhasoft.support.chat.ui.room.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.annotation.AttrRes
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.ilhasoft.support.chat.R
import br.com.ilhasoft.support.chat.helpers.ImageViewBindings
import br.com.ilhasoft.support.chat.models.ParseChat
import br.com.ilhasoft.support.chat.models.ParseMessage
import br.com.ilhasoft.support.chat.ui.media.MediaViewDelegate
import br.com.ilhasoft.support.chat.ui.room.ChatMessagesAdapter
import br.com.ilhasoft.support.chat.ui.room.activity.ChatRoomActivityContract
import br.com.ilhasoft.support.core.helpers.DimensionHelper
import br.com.ilhasoft.support.core.helpers.TintHelper
import br.com.ilhasoft.support.core.text.EmptyTextWatcher
import br.com.ilhasoft.support.media.MediaSelectorDelegate
import br.com.ilhasoft.support.recyclerview.decorations.LinearSpaceItemDecoration
import com.parse.ParseUser
import kotlinx.android.synthetic.main.fragment_chat_room.*
import rx.Observable
import rx.schedulers.Schedulers
import tgio.parselivequery.BaseQuery
import tgio.parselivequery.LiveQueryClient
import tgio.parselivequery.LiveQueryEvent
import tgio.parselivequery.Subscription

/**
 * Created by john-mac on 2/16/17.
 */
class ChatRoomFragment : Fragment(), ChatRoomContract {

    val chatId: String? by lazy { arguments?.getString(EXTRA_CHAT_ID) }
    var chat: ParseChat? = null
        set(value) {
            field = value
            presenter.chat = value
        }
    val mediaViewDelegate: MediaViewDelegate by lazy { MediaViewDelegate(context, fragmentManager) }
    val presenter: ChatRoomPresenter by lazy { ChatRoomPresenter(chat, chatClass, messageClass, context) }
    val messagesAdapter: ChatMessagesAdapter by lazy { ChatMessagesAdapter(mediaViewDelegate, myMessageBackground,
            otherMessageBackground, userNameKey) }
    val type: String? by lazy { arguments?.getString(EXTRA_TYPE) }
    var chatRoomActivityContract: ChatRoomActivityContract? = null

    /** Interface attributes collected from args **/
    val userNameKey: String? by lazy { arguments?.getString(ARG_USER_NAME_KEY) }
    val toolbarColor: Int by lazy { getColorArgOrPrimary(ARG_TOOLBAR_COLOR) }
    val attachMediaIcon: Int by lazy { getResourceOrDefault(ARG_ATTACH_MEDIA_ICON, R.drawable.ic_attach_file_36dp) }
    val sendMessageColor: Int by lazy { getColorArgOrPrimary(ARG_SEND_MESSAGE_COLOR) }
    val sendMessageIcon: Int by lazy { getResourceOrDefault(ARG_SEND_MESSAGE_ICON, R.drawable.ic_send_36dp) }
    val sendMessageHint: Int by lazy { getResourceOrDefault(ARG_SEND_MESSAGE_HINT, R.string.hint_message) }
    val myMessageBackground: Int by lazy { getResourceOrDefault(ARG_MY_MESSAGE_BACKGROUND, R.drawable.bubble_me) }
    val otherMessageBackground: Int by lazy { getResourceOrDefault(ARG_OTHER_MESSAGE_BACKGROUND, R.drawable.bubble_other) }
    @Suppress("UNCHECKED_CAST")
    val chatClass: Class<ParseChat> by lazy { arguments?.getSerializable(ARG_CHAT_CLASS) as? Class<ParseChat> ?: ParseChat::class.java }
    @Suppress("UNCHECKED_CAST")
    val messageClass: Class<ParseMessage> by lazy { arguments?.getSerializable(ARG_MESSAGE_CLASS) as? Class<ParseMessage> ?: ParseMessage::class.java }

    val mediaDelegate: MediaSelectorDelegate by lazy {
        MediaSelectorDelegate(context, "${context.packageName}.provider").apply {
            enableMultipleFiles()
            enableMultipleGalleryImages()
            setOnLoadAudioListener(presenter.onLoadAudio)
            setOnLoadMultipleFilesListener(presenter.onLoadFile)
            setOnLoadGalleryMultipleImagesListener(presenter.onLoadGalleryImage)
            setOnLoadImageListener(presenter.onLoadImage)
            setOnLoadVideoListener(presenter.onLoadVideo)
            setOnPickYoutubeVideoListener(presenter.onLoadYoutube)
        }
    }

    private var subscription: Subscription? = null

    companion object {
        const val TAG = "ChatRoom"
        const val EXTRA_TYPE = "type"
        const val EXTRA_CHAT = "chat"
        const val EXTRA_CHAT_ID = "chatId"

        const val EXTRA_USER1 = "user1"
        const val EXTRA_USER2 = "user2"

        const val TYPE_WITHOUT_CHAT = "withoutChat"
        const val TYPE_CHAT_LOADED = "chatLoaded"
        const val TYPE_CHAT_NOT_LOADED = "chatNotLoaded"
        const val TYPE_USERS = "users"

        const val ARG_USER_NAME_KEY = "userNameKey"
        const val ARG_USER_PICTURE_KEY = "pictureKey"
        const val ARG_USER_DEFAULT_PICTURE = "userDefaultPictureDrawable"
        const val ARG_GROUP_DEFAULT_PICTURE = "groupDefaultPictureDrawable"
        const val ARG_TOOLBAR_COLOR = "toolbarColor"
        const val ARG_ATTACH_MEDIA_ICON = "attachMediaIcon"
        const val ARG_SEND_MESSAGE_COLOR = "sendMessageColor"
        const val ARG_SEND_MESSAGE_ICON = "sendMessageIcon"
        const val ARG_SEND_MESSAGE_HINT = "sendMessageHint"
        const val ARG_MY_MESSAGE_BACKGROUND = "myMessageBackground"
        const val ARG_OTHER_MESSAGE_BACKGROUND = "otherMessageBackground"
        const val ARG_SHOW_CHAT_INFO = "showChatInfo"
        const val ARG_SHOW_EDIT_GROUP = "showEditGroup"
        const val ARG_CHAT_CLASS = "chatClass"
        const val ARG_MESSAGE_CLASS = "messageClass"

        const val RESOURCE_NOT_FOUND = 0

        var open = false

        fun newInstance(chat: ParseChat?, args: Bundle): ChatRoomFragment {
            args.putParcelable(EXTRA_CHAT, chat)
            args.putString(EXTRA_TYPE, TYPE_CHAT_LOADED)

            val fragment = ChatRoomFragment()
            fragment.arguments = args
            return fragment
        }

        fun newInstance(chatId: String?, args: Bundle): ChatRoomFragment {
            args.putString(EXTRA_CHAT_ID, chatId)
            args.putString(EXTRA_TYPE, TYPE_CHAT_NOT_LOADED)

            val fragment = ChatRoomFragment()
            fragment.arguments = args
            return fragment
        }

        fun newInstance(user1: ParseUser?, user2: ParseUser?, args: Bundle): ChatRoomFragment {
            args.putString(EXTRA_USER1, user1?.objectId)
            args.putString(EXTRA_USER2, user2?.objectId)
            args.putString(EXTRA_TYPE, TYPE_USERS)

            val fragment = ChatRoomFragment()
            fragment.arguments = args
            return fragment
        }

        internal fun newInstance(args: Bundle): ChatRoomFragment {
            val fragment = ChatRoomFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        presenter.attach(this)
        return inflater?.inflate(R.layout.fragment_chat_room, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        loadChat()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        chatRoomActivityContract = context as? ChatRoomActivityContract
    }

    fun getColorArgOrPrimary(arg: String): Int {
        val res = getResourceOrDefault(arg, RESOURCE_NOT_FOUND)
        return if (res != RESOURCE_NOT_FOUND) ContextCompat.getColor(context, res)
        else getThemeColor(R.attr.colorPrimary)
    }

    private fun getResourceOrDefault(argKey: String, defaultValue: Int) =
            arguments?.getInt(argKey, defaultValue) ?: defaultValue

    fun getThemeColor(@AttrRes attr: Int): Int {
        val value = TypedValue()
        context.theme.resolveAttribute(attr, value, true)
        return value.data
    }

    private fun loadChat() {
        when (type) {
            TYPE_CHAT_LOADED ->  {
                chat = arguments?.getParcelable<ParseChat>(EXTRA_CHAT)
                presenter.loadChat(chat)
            }
            TYPE_CHAT_NOT_LOADED -> presenter.loadChatById(chatId!!)
            TYPE_USERS -> presenter.loadChatWithUsers(getUser(EXTRA_USER1), getUser(EXTRA_USER2))
        }
    }

    fun loadChat(chat: ParseChat) {
        this.chat = chat
        presenter.loadChat(chat)
    }

    fun loadChatId(chatId: String) {
        presenter.loadChatById(chatId)
    }

    private fun getUser(key: String) = ParseUser.createWithoutData(ParseUser::class.java,
            arguments?.getString(key))

    private fun setupView() {
        supportChatRefreshLayout.setColorSchemeColors(toolbarColor)

        supportChatMessages.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, true)
            val spaceItemDecoration = LinearSpaceItemDecoration(LinearLayoutManager.VERTICAL,
                                                                DimensionHelper.toPx(context, 10f))
            addItemDecoration(spaceItemDecoration)
            adapter = messagesAdapter
        }
        supportChatSend.setOnClickListener { presenter.onClickSendMessage(supportChatMessage) }

        TintHelper.tintImageView(supportChatSend, sendMessageColor)
        ImageViewBindings.loadVectorImageView(supportChatIcon, attachMediaIcon)
        supportChatMessage.apply {
            setHint(sendMessageHint)
            addTextChangedListener(object : EmptyTextWatcher() {
                override fun onTextChanged(sequence: CharSequence?, start: Int, before: Int, count: Int) {
                    if (!TextUtils.isEmpty(sequence)) {
                        ImageViewBindings.loadVectorImageView(supportChatIcon, sendMessageIcon)
                    } else {
                        ImageViewBindings.loadVectorImageView(supportChatIcon, attachMediaIcon)
                    }
                }
            })
        }
    }

    private fun startSubscription() {
        stopSubscription()
            .flatMap { createSubscription() }
            .subscribeOn(Schedulers.computation())
            .subscribe({ Log.i(TAG, "Started subscription succesffully!") },
                { Log.i(TAG, "Error starting subscription", it) })
    }

    private fun createSubscription(): Observable<Subscription>? {
        return Observable.fromCallable<Subscription> {
            Log.i(TAG, "Call startSubscription")
            LiveQueryClient.connect()

            subscription = BaseQuery.Builder(messageClass.newInstance().className)
                    .where(ParseMessage::chatId.name, chat?.objectId)
                    .addField(ParseMessage::text.name)
                    .addField(ParseMessage::media.name)
                    .addField(ParseMessage::user.name)
                    .addField(ParseMessage::chatId.name)
                    .build()
                    .subscribe()
            subscription?.on(LiveQueryEvent.CREATE, presenter)
            subscription
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mediaDelegate.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        mediaDelegate.onRequestPermissionResult(this, requestCode, grantResults)
    }

    override fun showMessage(messageId: Int) {
        supportChatMessage.post { Snackbar.make(content, messageId, Snackbar.LENGTH_LONG).show() }
    }

    override fun showMessage(message: CharSequence?) {
        supportChatMessage.post { Snackbar.make(content, message ?: "", Snackbar.LENGTH_LONG).show() }
    }

    override fun showLoading() {
        supportChatSend.isEnabled = false

        supportChatRefreshLayout.isEnabled = true
        supportChatRefreshLayout.isRefreshing = true
    }

    override fun dismissLoading() {
        supportChatSend.isEnabled = true

        supportChatRefreshLayout.isRefreshing = false
        supportChatRefreshLayout.isEnabled = false
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
        open = true
    }

    override fun onStop() {
        super.onStop()
        presenter.stop()
        open = false
    }

    override fun onDestroy() {
        super.onDestroy()
        disconnectAndUnsubscribe()
        presenter.detachView()
    }

    private fun disconnectAndUnsubscribe() {
        Log.i(TAG, "Call disconnectAndUnsubscribe()")
        stopSubscription()
            .flatMap { createLiveQueryDisconnect() }
            .subscribeOn(Schedulers.newThread()).subscribe(
                { Log.i(TAG, "Disconnected and unsubscribed!") },
                { Log.e(TAG, "Error disconnecting or unsubscribing", it) })
    }

    private fun createLiveQueryDisconnect(): Observable<Subscription> {
        return Observable.fromCallable<Subscription> {
            if (!LiveQueryClient.hasSubscriptions()) {
                LiveQueryClient.disconnect()
            }
            subscription
        }
    }

    private fun stopSubscription(): Observable<Subscription> {
        return if (subscription != null) {
            Observable.fromCallable<Subscription> {
                subscription?.unsubscribe()
                subscription
            }.doOnNext { Log.i(TAG, "Stopped subscription!") }
        } else {
            Observable.just(subscription)
        }
    }

    override fun pickMediaMessage() {
        mediaDelegate.selectMedia(this)
    }

    override fun setMessages(messages: MutableList<ParseMessage>) {
        messagesAdapter.messages = messages
    }

    override fun addMessage(message: ParseMessage?) {
        message?.let {
            messagesAdapter.addMessage(it)
            supportChatMessages.smoothScrollToPosition(0)
        }
    }

    override fun clearMessage() {
        supportChatMessage.text = null
    }

    override fun setupChat(chat: ParseChat) {
        this.chat = chat
        this.chatRoomActivityContract?.setupChat(chat)
        this.startSubscription()

        presenter.loadMessages()
    }

    override fun getFragmentContext(): Context = context

}