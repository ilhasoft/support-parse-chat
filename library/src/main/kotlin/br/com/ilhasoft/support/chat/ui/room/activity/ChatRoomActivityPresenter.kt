package br.com.ilhasoft.support.chat.ui.room.activity

import br.com.ilhasoft.support.chat.R
import br.com.ilhasoft.support.chat.helpers.ObservableHelper
import br.com.ilhasoft.support.chat.models.ParseChat
import br.com.ilhasoft.support.core.mvp.Presenter
import com.parse.ParseFile
import com.parse.ParseUser
import rx.parse.ParseObservable

/**
 * Created by john-mac on 2/16/17.
 */
class ChatRoomActivityPresenter(var chat: ParseChat?) :
        Presenter<ChatRoomActivityContract>(ChatRoomActivityContract::class.java) {

    fun attach(view: ChatRoomActivityContract): ChatRoomActivityPresenter {
        super.attachView(view)
        return this
    }

    fun onClickInformation() {
        view?.startChatGroupInfo()
    }

    fun onClickEditChat() {
        view?.startChatGroupEdit()
    }

    fun onClickDeleteChat() {
        view?.requestConfirmation(R.string.message_delete_chat)?.subscribe { if (it) deleteChat() }
    }

    fun onClickExitChat() {
        view?.requestConfirmation(R.string.message_exit_chat)?.subscribe { if (it) exitChat() }
    }

    private fun deleteChat() {
        ParseObservable.delete(chat)
                .compose { ObservableHelper.transformer(it, this) }
                .subscribe({ view?.finish() }, { ObservableHelper.error(this, it) })
    }

    private fun exitChat() {
        ParseChat.removeUserFromGroupChat(chat, ParseUser.getCurrentUser())
                .compose { ObservableHelper.transformer(it, this) }
                .subscribe ({ view?.finish() }, { ObservableHelper.error(this, it) })
    }

    fun getChatName(chat: ParseChat, userNameKey: String?): String? {
        if (chat.isIndividual() && userNameKey?.isNotEmpty() == true) {
            val user = chat.users?.filter { it != ParseUser.getCurrentUser() }?.firstOrNull()
            if (user != null && user.has(userNameKey)) {
                return user.getString(userNameKey)
            }
        }
        return chat.title
    }

    fun getChatPicture(chat: ParseChat, userPictureKey: String?): ParseFile? {
        var pictureFile: ParseFile? = null
        if (chat.isIndividual()) {
            val user = chat.users?.filter { it != ParseUser.getCurrentUser() }?.firstOrNull()
            if (user != null && user.has(userPictureKey)) {
                pictureFile = user.getParseFile(userPictureKey)
            }
        } else {
            pictureFile = chat.media
        }
        return pictureFile
    }

    fun hasAdminPrivileges() = chat?.administrator == ParseUser.getCurrentUser()

    fun isGroupChat() = chat?.isIndividual() == false

}