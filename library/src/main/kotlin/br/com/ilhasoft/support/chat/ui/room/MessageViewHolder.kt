package br.com.ilhasoft.support.chat.ui.room

import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.widget.RelativeLayout
import br.com.ilhasoft.support.chat.R
import br.com.ilhasoft.support.chat.models.ParseMessage
import kotlinx.android.synthetic.main.item_text_chat_message.view.*

/**
 * Created by john-mac on 12/14/16.
 */
abstract class MessageViewHolder(val view: View, val myMessageBackground: Int, val
        otherMessageBackground: Int, val userNameKey: String?) : RecyclerView.ViewHolder(view) {

    open fun bind(message: ParseMessage) {
        val bubbleParams = view.bubble.layoutParams as RelativeLayout.LayoutParams
        bubbleParams.addRule(if (message.isMyMessage()) RelativeLayout.ALIGN_PARENT_RIGHT
            else RelativeLayout.ALIGN_PARENT_LEFT)
        bubbleParams.addRule(if (message.isMyMessage()) RelativeLayout.ALIGN_PARENT_LEFT
        else RelativeLayout.ALIGN_PARENT_RIGHT, 0)

        view.bubble.apply {
            layoutParams = bubbleParams
            gravity = if (message.isMyMessage()) Gravity.END else Gravity.START
            setBackgroundResource(if (message.isMyMessage()) myMessageBackground else otherMessageBackground)
        }
        if (!message.isMyMessage()) {
            view.name.visibility = View.VISIBLE
            view.name.text = message.user?.getString(userNameKey)
        } else {
            view.name.visibility = View.GONE
        }
        view.time.apply {
            text = message.getTimeFormatted()
            setTextColor(getColor(message))
        }
    }

    fun getColor(message: ParseMessage) = ResourcesCompat.getColor(itemView.context.resources,
            getTextColor(message), itemView.context.theme)

    private fun getTextColor(message: ParseMessage) = if (message.isMyMessage()) android.R.color.white
        else R.color.warm_gray_two
}