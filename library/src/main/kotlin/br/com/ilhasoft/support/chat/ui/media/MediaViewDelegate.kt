package br.com.ilhasoft.support.chat.ui.media

import android.content.Context
import android.support.v4.app.FragmentManager
import br.com.ilhasoft.support.core.helpers.FileHelper
import br.com.ilhasoft.support.media.RecordAudioFragment
import br.com.ilhasoft.support.media.YoutubeHelper
import br.com.ilhasoft.support.media.view.MediaModel
import br.com.ilhasoft.support.media.view.MediaViewOptions
import br.com.ilhasoft.support.media.view.models.ImageMedia
import br.com.ilhasoft.support.media.view.models.VideoMedia
import br.com.ilhasoft.support.media.view.models.YouTubeMedia
import br.com.ilhasoft.support.parse.models.ParseMedia
import java.util.*

/**
 * Created by john-mac on 2/15/17.
 */
class MediaViewDelegate(val context: Context, val supportFragmentManager: FragmentManager) : MediaAdapter.OnMediaViewListener {

    override fun onMediaView(medias: MutableList<ParseMedia>?, position: Int) {
        var index = position
        val mediaModels = ArrayList<MediaModel>()
        medias?.forEachIndexed { i, media ->
            val mediaModel = (when (media.type) {
                ParseMedia.TYPE_YOUTUBE -> YouTubeMedia(YoutubeHelper.getVideoUrlFromId(media.youtubeCode))
                ParseMedia.TYPE_VIDEO -> VideoMedia(media.mediaFile?.url, media.thumbnail?.url)
                ParseMedia.TYPE_PICTURE -> ImageMedia(media.mediaFile?.url)
                else -> { if (i < position) index--; null }
            })
            mediaModel?.let { mediaModels.add(it) }
        }

        context.startActivity(MediaViewOptions(mediaModels, index)
                .setTitleEnabled(false)
                .setBackgroundRes(android.R.color.black)
                .createIntent(context))
    }

    override fun onFileMediaView(media: ParseMedia?) {
        val fileIntent = FileHelper.createFileOpenIntent(media?.mediaFile?.url)
        context.startActivity(fileIntent)
    }

    override fun onAudioMediaView(media: ParseMedia?) {
        val audioPlayer = RecordAudioFragment.newInstance(media?.mediaFile?.url)
        audioPlayer.show(supportFragmentManager, "audioPlayer")
    }

}