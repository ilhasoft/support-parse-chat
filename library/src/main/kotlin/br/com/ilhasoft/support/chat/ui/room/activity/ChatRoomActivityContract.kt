package br.com.ilhasoft.support.chat.ui.room.activity

import android.support.annotation.StringRes
import br.com.ilhasoft.support.chat.models.ParseChat
import br.com.ilhasoft.support.core.mvp.BasicView
import rx.Observable

/**
 * Created by john-mac on 2/16/17.
 */
interface ChatRoomActivityContract : BasicView {

    fun startChatGroupInfo()

    fun startChatGroupEdit()

    fun requestConfirmation(@StringRes message: Int): Observable<Boolean>

    fun setupChat(chat: ParseChat)

    fun finish()

}