package br.com.ilhasoft.support.chat.helpers

import android.content.Context
import android.net.Uri
import br.com.ilhasoft.support.chat.models.MediaMetadata
import br.com.ilhasoft.support.core.helpers.IoHelper
import br.com.ilhasoft.support.core.mvp.Presenter
import br.com.ilhasoft.support.media.MediaSelectorDelegate
import br.com.ilhasoft.support.media.YoutubeHelper
import br.com.ilhasoft.support.parse.helpers.MediaHelper
import br.com.ilhasoft.support.parse.models.ParseFile
import br.com.ilhasoft.support.parse.models.ParseMedia
import rx.Observable
import java.io.File

/**
 * Created by john-mac on 1/31/17.
 */
abstract class ParseMediaPresenter<View>(viewClass: Class<View>, val context: Context?) : Presenter<View>(viewClass) {

    protected fun saveMedia(context: Context?, media: ParseMedia?): Observable<ParseMedia> {
        return Observable.just(media).compose { MediaHelper.saveMedia(context, it) }
    }

    protected fun saveMedias(context: Context?, medias: List<ParseMedia>?): Observable<MutableList<ParseMedia>> {
        return Observable.just(medias)
                .concatMap { getSingleMediaObservable(it) }
                .compose { MediaHelper.saveMedia(context, it) }.toList()
    }

    protected fun getSingleMediaObservable(medias: List<ParseMedia>?): Observable<ParseMedia> {
        return if (medias == null) {
            Observable.empty<ParseMedia>()
        } else {
            Observable.from(medias)
                    .filter { it.isDirty }
        }
    }

    val onLoadAudio = MediaSelectorDelegate.OnLoadAudioListener { uri, duration ->
        val parseMedia = ParseMedia()
        parseMedia.type = ParseMedia.TYPE_AUDIO
        parseMedia.mediaFile = ParseFile(File(IoHelper.getFilePathForUri(context, uri)))
        parseMedia.metadata = mapOf(Pair(MediaMetadata.AUDIO_DURATION, duration.toString()))
        addMedia(parseMedia)
    }

    val onLoadFile = MediaSelectorDelegate.OnLoadMultipleMediaListener { uriList ->
        uriList?.forEach { uri ->
            val parseMedia = ParseMedia()
            parseMedia.type = ParseMedia.TYPE_FILE
            parseMedia.mediaFile = ParseFile(File(IoHelper.getFilePathForUri(context, uri)))
            parseMedia.metadata = mapOf(Pair(MediaMetadata.FILENAME, parseMedia.mediaFile.name))
            addMedia(parseMedia)
        }
    }

    val onLoadGalleryImage = MediaSelectorDelegate.OnLoadMultipleMediaListener { uriList ->
        uriList?.forEach { uri ->
            addPictureUri(uri)
        }
    }

    val onLoadImage = MediaSelectorDelegate.OnLoadMediaListener { uri ->
        addPictureUri(uri)
    }

    private fun addPictureUri(uri: Uri?) {
        addMediaFile(uri, ParseMedia.TYPE_PICTURE)
    }

    val onLoadVideo = MediaSelectorDelegate.OnLoadMediaListener { uri ->
        addMediaFile(uri, ParseMedia.TYPE_VIDEO)
    }

    private fun addMediaFile(uri: Uri?, type: String) {
        val parseMedia = ParseMedia()
        parseMedia.type = type
        parseMedia.mediaFile = ParseFile(File(IoHelper.getFilePathForUri(context, uri)))
        addMedia(parseMedia)
    }

    val onLoadYoutube = YoutubeHelper.OnPickYoutubeVideoListener { youtubeCode, _ ->
        val parseMedia = ParseMedia()
        parseMedia.type = ParseMedia.TYPE_YOUTUBE
        parseMedia.youtubeCode = youtubeCode
        addMedia(parseMedia)
    }

    abstract fun addMedia(media: ParseMedia)
}