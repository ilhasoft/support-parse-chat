package br.com.ilhasoft.support.chat.ui.room

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import br.com.ilhasoft.support.chat.R
import br.com.ilhasoft.support.chat.models.ParseMessage
import br.com.ilhasoft.support.chat.ui.media.MediaAdapter
import java.util.*

/**
 * Created by john-mac on 12/13/16.
 */
class ChatMessagesAdapter(val onMediaViewListener: MediaAdapter.OnMediaViewListener,
                          val myMessageBackground: Int, val otherMessageBackground: Int, val userNameKey: String?)
    : RecyclerView.Adapter<MessageViewHolder>() {

    init { setHasStableIds(true) }

    companion object {
        private const val VIEW_TYPE_TEXT_MESSAGES = 0
        private const val VIEW_TYPE_MEDIA_MESSAGES = 1
    }

    var messages: MutableList<ParseMessage> = ArrayList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MessageViewHolder {
        val inflater = LayoutInflater.from(parent?.context)
        return when(viewType) {
            VIEW_TYPE_TEXT_MESSAGES -> TextMessageViewHolder(inflater.inflate(R.layout.item_text_chat_message,
                    parent, false), myMessageBackground, otherMessageBackground, userNameKey)
            else -> MediaMessageViewHolder(inflater.inflate(R.layout.item_media_chat_message, parent, false)
                    , onMediaViewListener, myMessageBackground, otherMessageBackground, userNameKey)
        }
    }

    override fun onBindViewHolder(holder: MessageViewHolder?, position: Int) {
        holder?.bind(messages[position])
    }

    override fun getItemCount(): Int = messages.size

    override fun getItemViewType(position: Int): Int {
        val message = messages[position]
        return if (message.media != null) VIEW_TYPE_MEDIA_MESSAGES
                else VIEW_TYPE_TEXT_MESSAGES
    }

    override fun getItemId(position: Int): Long {
        return messages[position].objectId?.hashCode()?.toLong() ?: Long.MAX_VALUE
    }

    fun addMessage(message: ParseMessage) {
        messages.add(0, message)
        notifyDataSetChanged()
    }

    fun setMessage(message: ParseMessage) {
        val position = messages.indexOf(message)
        if (position >= 0) {
            messages[position] = message
            notifyItemChanged(position)
        }
    }

}